#### [权限控制的五张表]()
关于用户鉴权的几张表
```
用户表
角色表
用户角色关联表
菜单表
菜单角色关联表
```

#### 表结构设计如下

##### （1）表个数

```
t_menu
t_role
t_role_menu
t_role_user
t_user
```

##### （2）用户表

​	show create table t_user

```
CREATE TABLE `t_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL COMMENT '状态，是否为禁用状态。其中status为0表示禁用。1表示启用状态',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
```

select * from t_user

| user_id | username | password | status |
| ------- | -------- | -------- | ------ |
| 1       | knight   | knight   | 1      |
| 2       | tom      | tom      | 1      |
| 3       | rose     | rose     | 1      |
| 4       | nick     | nick     | 0      |

##### （2）角色表

```
CREATE TABLE `t_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(4) NOT NULL COMMENT '从1000开始：\r\n1001： 表示管理员\r\n1002： 普通管理员\r\n1003： 普通用户\r\n',
  `role_name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
```

SELECT * from t_role

| id   | role_id | role_name  |
| ---- | ------- | ---------- |
| 1    | 1001    | 超级管理员 |
| 2    | 1002    | 普通管理员 |
| 3    | 1003    | 普通用户   |

##### （3）角色和用户关联表

```
CREATE TABLE `t_role_user` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL,
  `role_id` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
```

select * from t_role_user

| id   | user_id | role_id |
| ---- | ------- | ------- |
| 1    | 1       | 1001    |
| 2    | 2       | 1002    |
| 3    | 3       | 1003    |
| 4    | 4       | 1003    |

##### （4）菜单表

其中`status` 只有0和1两种状态。0表示没有权限，1表示有权限

```
CREATE TABLE `t_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
```

SELECT * from t_menu

| id   | name     | path          | status |
| ---- | -------- | ------------- | ------ |
| 1    | 菜单管理 | /menu         | 1      |
| 2    | 角色管理 | /role         | 1      |
| 3    | 员工管理 | /staff        | 1      |
| 4    | 活动列表 | /ActivityList | 1      |

##### （5）菜单角色管理表

假如有5个菜单，3个角色的话，那么这个表里的记录就有15条，其中`status` 表示是否有权限。所以在查询是否有记录的时候还要查询状态是否为1。

```
CREATE TABLE `t_role_menu` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `role_id` int(4) NOT NULL,
  `menu_id` int(4) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
```

SELECT * from t_role_menu

| id   | role_id | menu_id | status |
| ---- | ------- | ------- | ------ |
| 1    | 1001    | 1       | 1      |
| 2    | 1001    | 2       | 1      |
| 3    | 1001    | 3       | 1      |
| 4    | 1002    | 1       | 1      |
| 5    | 1002    | 2       | 1      |

#### 流程分析

##### （1） 当超级管理员新建用户的时候，哪些表有更新?

当超级管理员新建用户的时候，一定要指定要用户所属的角色。所以在创建用户的时候会在用户表（t_user）里创建记录。同时会在用户角色管理表（t_role_user）里创建记录。

##### （2）超级管理员新建用户角色的时候做了一些什么？

当超级管理员，新建用户角色（也就是创建用户组）的时候，只会在角色表（t_role）表里创建记录。

##### （3）超级管理员在创建用户角色之后分配菜单权限的时候做了一些什么？

会在角色菜单管理表（t_role_menu）里创建记录。理论上上来讲，有多少个菜单按钮就有多少条记录。`status`字段用来区分是否有权限。

##### （4）在前后端分离的设计中吗，在用户登录的时候，做了一些什么？

**方式一**： 首先后端拿到前端传过来的用户名和密码。首先去查用户表（t_user表），用户名和密码是否能对上。如果能对上的话。就去查用户角色表（t_role_user表），得到用户所属的角色，再去查角色和菜单管理表（t_role_menu表）有哪些菜单权限，然后把拥有的菜单结果存到数组里返回给前端。

 也就是说用户在登录的时候，如果用户名和密码正确的话，是查了三次表。当然你可以一条 sql 语句搞定，关联查询就可以了。

其实1张表或者2张表就可以搞定，但是为了能更好的扩张，所以分5张表去做这个事情。

**方式二：** 用户登录的时候，后端通过用户名和密码查用户表和用户角色表得到角色id，然后把数据返回给前端。验证用户名和密码验证通过了前端再法一次请求到后端拿 用户的菜单信息。（虽然前端只点击了一次button，但是可以发送两次请求）



##### 连表查询

（1）通过用户名查询用户表，然后再查用户角色表得到角色表信息。

```
select * from api_t_role_user ru, api_t_user u where ru.user_id = u.user_id and u.username = "tom";
```

 

（2）通过查找角色菜单表，然后再查菜单表，得到拥有的菜单信息。

```
-- 通过角色id找到对应的菜单权限信息
select * from api_t_menu m where m.id in (select r.menu_id from api_t_role_menu r where role_id = 1002)
```

