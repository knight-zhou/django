/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : db_auth

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 18/09/2020 12:04:23
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for api_t_menu
-- ----------------------------
DROP TABLE IF EXISTS `api_t_menu`;
CREATE TABLE `api_t_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `status` int(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of api_t_menu
-- ----------------------------
INSERT INTO `api_t_menu` VALUES (1, ' 菜单管理', '/menu', 1);
INSERT INTO `api_t_menu` VALUES (2, '角色管理', '/role', 1);
INSERT INTO `api_t_menu` VALUES (3, '员工管理', '/staff', 1);
INSERT INTO `api_t_menu` VALUES (4, '活动列表', '/ActivityList', 1);

-- ----------------------------
-- Table structure for api_t_role
-- ----------------------------
DROP TABLE IF EXISTS `api_t_role`;
CREATE TABLE `api_t_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(4) NOT NULL COMMENT '从1000开始：\r\n1001： 表示管理员\r\n1002： 普通管理员\r\n1003： 普通用户\r\n',
  `role_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of api_t_role
-- ----------------------------
INSERT INTO `api_t_role` VALUES (1, 1001, '超级管理员');
INSERT INTO `api_t_role` VALUES (2, 1002, '普通管理员');
INSERT INTO `api_t_role` VALUES (3, 1003, '普通用户');

-- ----------------------------
-- Table structure for api_t_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `api_t_role_menu`;
CREATE TABLE `api_t_role_menu`  (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `role_id` int(4) NOT NULL,
  `menu_id` int(4) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of api_t_role_menu
-- ----------------------------
INSERT INTO `api_t_role_menu` VALUES (1, 1001, 1, 1);
INSERT INTO `api_t_role_menu` VALUES (2, 1001, 2, 1);
INSERT INTO `api_t_role_menu` VALUES (3, 1001, 3, 1);
INSERT INTO `api_t_role_menu` VALUES (4, 1002, 1, 1);
INSERT INTO `api_t_role_menu` VALUES (5, 1002, 2, 1);
INSERT INTO `api_t_role_menu` VALUES (6, 1002, 3, 1);

-- ----------------------------
-- Table structure for api_t_role_user
-- ----------------------------
DROP TABLE IF EXISTS `api_t_role_user`;
CREATE TABLE `api_t_role_user`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL,
  `role_id` int(4) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of api_t_role_user
-- ----------------------------
INSERT INTO `api_t_role_user` VALUES (1, 1, 1001);
INSERT INTO `api_t_role_user` VALUES (2, 2, 1002);

-- ----------------------------
-- Table structure for api_t_user
-- ----------------------------
DROP TABLE IF EXISTS `api_t_user`;
CREATE TABLE `api_t_user`  (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL COMMENT '状态，是否为禁用状态。其中status为0表示禁用。1表示启用状态',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of api_t_user
-- ----------------------------
INSERT INTO `api_t_user` VALUES (1, 'knight', 'knight_p', 1);
INSERT INTO `api_t_user` VALUES (2, 'tom', 'tom_p', 1);
INSERT INTO `api_t_user` VALUES (3, 'rose', 'rose_p', 1);
INSERT INTO `api_t_user` VALUES (4, 'nick', 'nick_p', 0);

SET FOREIGN_KEY_CHECKS = 1;
