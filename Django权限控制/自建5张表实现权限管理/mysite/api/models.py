from django.db import models

# Create your models here.
# 用户表模型
class T_User(models.Model):
    user_id = models.AutoField(primary_key=True)
    username = models.CharField(max_length=10,null=False)
    password = models.CharField(max_length=10,null=False)
    status = models.IntegerField(null=False)

# 角色表模型
class T_Role(models.Model):
    id = models.AutoField(primary_key=True)
    role_id = models.IntegerField(null=False)
    role_name = models.CharField(max_length=11,null=False)


## 用户角色表
class T_Role_Use(models.Model):
    id = models.AutoField(primary_key=True)
    user_id = models.IntegerField(null=False)
    role_id = models.IntegerField(null=False)

# 菜单表
class T_Menu(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=11,null=False)
    path = models.CharField(max_length=11,null=False)
    stats = models.IntegerField(null=False)

# 角色菜单表
class T_Role_Menu(models.Model):
    id = models.AutoField(primary_key=True)
    role_id = models.IntegerField(null=False)
    menu_id = models.IntegerField(null=False)
    stats = models.IntegerField(null=False)












