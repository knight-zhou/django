from django.shortcuts import render
from django.http import HttpResponse
from .models import  T_Menu,T_Role,T_Role_Menu,T_Role_Use,T_User
from django.db import connection

# Create your views here.
def index(request):
    return HttpResponse("This is api interface!")

def login(request):
    # 用户登陆相关
    if request.method == "POST":
        name = request.POST.get('name', None)  # 避免提交空时异常
        pwd = request.POST.get('pwd', None)
        # print(name)
        # 查用户表
        ret_record = T_User.objects.filter(username=name).first()
        # print(ret_record.username)

        sql = "select * from api_t_role_user ru, api_t_user u where ru.user_id = u.user_id and u.username = 'tom'"
        if name == ret_record.username and pwd == ret_record.password:
            with connection.cursor() as cursor:
                cursor.execute(sql)
                row = cursor.fetchone()
                print(row)
            return HttpResponse("登陆成功")
        else:
            return HttpResponse("用户名或者密码不正确")

    return HttpResponse("请使用post提交数据.....")