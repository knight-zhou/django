#!/usr/bin/env python
# coding:utf-8
# from __future__ import unicode_literals
import sys
reload(sys)
sys.setdefaultencoding('utf8')

from django.db import models
from django.contrib.auth.models import User

#自定义用户表
class Userinfo(models.Model):
    user = models.OneToOneField(User) #关联django user表
    username = models.CharField(max_length=100)
    password = models.CharField(max_length=100)


    def __unicode__(self):
        return self.username



class quanxian(models.Model):
    shuoming=models.CharField(max_length=100)

    def __unicode__(self):
        return self.shuoming
    class Meta:
        permissions = (
            ('edit', u'编辑权限'),
            ('add', u'添加权限'),
            ('DEL',u'删除权限'),
            ('list',u'查看权限'),
        )