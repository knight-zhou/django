# coding:utf-8
from django.shortcuts import render,render_to_response,HttpResponse
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

from test01.models import Userinfo
from django import forms
from django.contrib import auth   # 导入dj自带的权限表
from django.contrib.auth.models import User  # 导入django自带的用户表
from django.contrib.auth import authenticate
from django.template import RequestContext


# Create your views here.
class UserForm(forms.Form):
    username = forms.CharField(label="user",max_length=100)
    password = forms.CharField(label="passwd",widget=forms.PasswordInput())
def index(request):
    return render_to_response('index.html')


def login(request):
    if request.method == 'POST':
        uf = UserForm(request.POST)
        if uf.is_valid():

            username = uf.cleaned_data['username']
            password = uf.cleaned_data['password']
            print username,password,"############"

            #user = Userinfo.objects.filter(username__exact = username,password__exact = password)
            ##print user,"......"

            user1 = authenticate(username=username, password=password)    # 验证是否存在
            is_add = True if user1.has_perm('test01.add') else False      # has_perm('')：判断用户是否具有特定权限,perm的格式是appname.codename(codename为数据库字段)
            #print 'user1--->',user1,user1.has_perm('test01.add'), is_add
            if user1:
                return render_to_response('index.html',locals(), context_instance=RequestContext(request))
            else:
                return HttpResponseRedirect('/login/')
    else:
        uf = UserForm()
    return render_to_response('login.html',{'uf':uf})



