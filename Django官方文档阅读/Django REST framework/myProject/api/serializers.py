from rest_framework import serializers
from .models import Student

class StudentSerializers(serializers.ModelSerializer):
    class Meta:
        model = Student  # 指定模型类

        fields = ('id', 'name', 'sex',)   #需要序列化的字段

