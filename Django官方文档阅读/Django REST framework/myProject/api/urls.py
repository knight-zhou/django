from django.urls import path,include
from rest_framework import routers
from api import views

# 定义路由地址
route = routers.DefaultRouter()

# 视图函数是类的情况，需要注册新的app路由地址
route.register(r'student',views.StudentViewSet)

# 注册上一级的路由地址并添加
urlpatterns = [
    path('api/', include(route.urls)),
    path(r'list',views.test_list)    # 函数视图在这里进行使用。
]

