from rest_framework import viewsets
from .models import Student
from .serializers import StudentSerializers
from rest_framework.decorators import api_view
from rest_framework.response import Response

# Create your views here.

# 基于类的视图
class StudentViewSet(viewsets.ModelViewSet):
    # 指定结果集
    queryset = Student.objects.all()
    # 指定序列化的类
    serializer_class = StudentSerializers

# Rest_FrameWork 中基于函数的视图实现
@api_view(['GET','POST'])
def test_list(request):
    data_list = []
    all_data = Student.objects.all()
    for i in all_data:
        data_list.append(i.to_json())
    model_data = {"name":"knight","ctiy":"sz"}
    zidingyi_data = {"res":{"code":10001,"msg":data_list}}
    return Response(zidingyi_data)

