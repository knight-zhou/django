from django.db import models

# Create your models here.

class Student(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(u'姓名', max_length=100, default='no_name')
    sex = models.CharField(u'性别', max_length=50, default='male')

    def __unicode__(self):
        return '%d: %s' % (self.id, self.name)

    def to_json(self):
        return {
            "id":self.id,
            "name":self.name,
            "sex":self.sex
        }