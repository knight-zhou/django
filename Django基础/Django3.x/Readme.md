#### Django 中返回Json对象

（1）使用 用django内置的进行json序列化 实现

```
## get请求查询数据库的数据并返回json
### 方式一：用django内置的进行json序列化

def list_all(request):
    # 获取所有的数据对象
    queryset = Choice.objects.all()
    # 将数据序列化成json格式
    data = serializers.serialize('json',queryset=queryset)
    return HttpResponse(data)


```

(2) 返回json对象

```
### 方法二
def list_all_2(request):
    # 获取所有的数据对象,必须要接value，不然无法返回json数据
    queryset = Choice.objects.all().values('id','choice_text','votes')
    # 将数据序列化成json格式,date类型的数据不能直接系列化 ensure_ascii=False 修改乱码的现象
    ret = json.dumps(list(queryset),ensure_ascii=False)
    return HttpResponse(ret)
```

(3) 使用Django Restful进行json序列化

具体可参考 官方文档阅读