from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

def index(request):
    return render(request,'index.html')

def add(request):
    a = request.GET['a']
    b = request.GET['b']
    a = int(a)
    b = int(b)
    return HttpResponse(str(a + b))


def xx(request):
    request.session['username'] = 'knight'

    role_id='1'  # role_id 为0表示菜单隐藏,1表示菜单表示显示
    permi_id='0'  # 1表示有权限，按钮可以正常点并做出动作, 0 表示没有权限 按钮为禁用或者点不动的状态(测试用例如果role_id为0就变红)

    zidian = {"aa": "我是中国人", "role_id": role_id,"permi_id":permi_id}
    return render(request,'menu.html', zidian)    # 传入字典渲染到前端