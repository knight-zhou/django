from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

def index(request):
    return render(request,'index.html')

def add(request):
    a = request.GET['a']
    b = request.GET['b']
    a = int(a)
    b = int(b)
    return HttpResponse(str(a + b))


def xx(request):
    request.session['username'] = 'knight'

    role_id='1'  # role_id 为0 表示隐藏,1 表示显示
    zidian = {"aa": "我是中国人", "role_id": role_id}
    return render(request,'menu.html', zidian)    # 传入字典渲染到前端