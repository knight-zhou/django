# coding:utf-8
from django.contrib import admin
from django.urls import path
from test01 import  views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('in/', views.index),
    path('add/', views.add),
]
