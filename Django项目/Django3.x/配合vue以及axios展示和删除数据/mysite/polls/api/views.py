from django.http import HttpResponse
from polls.models import Question,Choice,Book
from django.core import serializers
from django.db import connection
from mysite import conn_db
import random
import json
import simplejson
# 局部csrs
from django.views.decorators.csrf import csrf_exempt

# Create your views here.

def index(request):
    return HttpResponse("hello!")


def xx(request):
    return HttpResponse("this is xx interface");


# post请求插入数据
@csrf_exempt
def wenti(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        votes_value=random.randint(1, 100)
        c = Choice(choice_text=username,votes=votes_value)
        c.save()
        print("通过post访问这个接口,并传入指定参数，就会保存数据")
        return HttpResponse("sucesss")
    else:
        return HttpResponse("请发送post请求.....")

## get请求查询数据库的数据并返回json
### 方式一：用django内置的进行json序列化

# 构造自己的json数据返回,查询数据库里的所有数据
def list_all(request):
    # 获取所有的数据对象
    queryset = Choice.objects.all()
    # 将数据序列化成json格式
    data = serializers.serialize('json',queryset=queryset)
    res_data={"code":0,"data":json.loads(data)}
    res_data_str = json.dumps(res_data)
    return HttpResponse(res_data_str)

### 方法二
@csrf_exempt
def list_all_2(request):
    if request.method == "POST":
        req = simplejson.loads(request.body)
        user_id = req["params"]['id']
        # print(user_id)
        Choice.objects.get(id=user_id).delete()
        return HttpResponse("删除数据成功.....")
    # 获取所有的数据对象,必须要接value，不然无法返回json数据
    queryset = Choice.objects.all().values('id','choice_text','votes')
    # 将数据序列化成json格式,date类型的数据不能直接系列化 ensure_ascii=False 修改乱码的现象
    ret = json.dumps(list(queryset),ensure_ascii=False)
    return HttpResponse(ret)

# 传参进行查询
def ques(request):
    get_votes = request.GET.get('votes')
    print(type(get_votes))
    queryset = Choice.objects.filter(votes=int(get_votes))
    data = serializers.serialize('json',queryset=queryset)
    return HttpResponse(data)

# 通过模型层来执行原生SQL查询------ 使用raw方式
def org_list(request):
    sql = "select * from polls_choice"
    # print(Choice.objects.raw(sql)[0])

    for xx in Choice.objects.raw(sql):
        print(xx)
    return HttpResponse("通过模型层来执行原生的SQL--- 使用raw方式，执行成功......")


## 不通过模型层来执行SQL查询
def my_custom_sql(self):
    with connection.cursor() as cursor:
        cursor.execute("select * from polls_choice")
        row = cursor.fetchone()
        print(row)
    return HttpResponse("不通过模型层执行SQL查询成功")

# 通过导入公共类实现自定义SQL语句
def sql_org(request):
    sql = "INSERT into polls_book (title) VALUES ('yy')"
    # 实例化
    db = conn_db.Conn_Mysql()
    db.exe_sql(sql)
    return HttpResponse("使用pymysql执行SQL语句")


## 通过实例方法进行传参
def ins(request):
    # book = Book.create("java")
    book = Book(title="ooo")
    book.save()
    return HttpResponse("实例化模型插入数据成功")

## 用户登录
def login(request):
    return "用户登录....."






