from django.shortcuts import render


# url: http://127.0.0.1:8000/polls/home/detail
## 渲染到模板，前端使用vue
def detail(request):
    return render(request,'detail.html',{"name":"tom"})

##
# url: http://127.0.0.1:8000/polls/home/user_list
# api: http://127.0.0.1:8000/static/json/ip.json

def user_list(request):
    return render(request,'user_list.html')

