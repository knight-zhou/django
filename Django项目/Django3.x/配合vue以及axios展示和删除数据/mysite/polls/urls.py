from django.urls import path
from polls.api import views
from .views_home import detail,user_list

urlpatterns = [
    path('', views.index),
    path('xx', views.xx),
    path('wenti', views.wenti),
    path('list', views.list_all),
    path('list2', views.list_all_2),
    path('ques', views.ques),
    path('org', views.org_list),
    path('row', views.my_custom_sql),
    path('ins', views.ins),
    path('sql_org', views.sql_org),
    path('home/detail',detail),
    path('home/user_list',user_list)
]
