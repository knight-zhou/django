#!/usr/bin/env python3
# from redis.sentinel import Sentinel
from redis import sentinel

sentinel.Sentinel = sentinel.Sentinel ([('sentinel-01-01.carsales.cn', 6000),
                                        ('sentinel-01-02.carsales.cn', 6000),
                                        ('sentinel-01-03.carsales.cn', 6000)
                                        ],socket_timeout=0.5)

##获取主服务器地址
master = sentinel.Sentinel.discover_master('hlloms_srv_prd')
print(master)

###获取从服务器地址
slave = sentinel.Sentinel.discover_slaves('hlloms_srv_prd')
# print(slave)



## 读取
# num =master.count("ra:login:info:24:JGcfwIYBa5kOuy2we9ZQPzuPfWuGQp9v")

xx = master.get("xx")
print(xx)
