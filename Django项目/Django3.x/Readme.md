#### 要点

多读官方文档，官方文档还有中文版的

官方文档： https://docs.djangoproject.com/zh-hans/3.1/



#### Django的模型的实例方法

官方文档： https://docs.djangoproject.com/zh-hans/3.1/ref/models/instances/

在我们的模型层，我们可以加实例方法。那样我们在视图层的时候可以通过调用实例方法做一些增删改查的操作。并且即便你用了实例方法操作数据。同事你也可以通过原生的ORM操作数据，他们并不冲突。

例如：

models.py

```python
## 创建实例方法
class Book(models.Model):
    title = models.CharField(max_length=100)

    @classmethod
    def create(cls,title):
        # book = cls(title=title)
        book = cls(title=title+"_xx")
        # 对这个实例加一些方法去实现
        book.save()
        return book
```

views .py

```
## 通过实例方法进行传参
def ins(request):
    book = Book.create("java")
    # book = Book(title="python")
    # book.save()
    return HttpResponse("实例化模型插入数据成功")
```

#### Django使用原生SQL的方法

模型层代码:

```
class Choice(models.Model):
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
            return self.choice_text+" "+str(self.votes)

```

##### （1）使用raw 方式

```
# 视图层代码
# 通过模型层来执行原生SQL查询------ 使用raw方式
def org_list(request):
    sql = "select * from polls_choice"
    # print(Choice.objects.raw(sql)[0])
   
    # 显示的数据内容和模型层定义的__str__方法有关  
    for xx in Choice.objects.raw(sql):
        print(xx)
    return HttpResponse("通过模型层来执行原生的SQL--- 使用raw方式，执行成功......")
```

##### （2） 使用 connection 方式

这种方式适合多表查询，在`raw` 方式 满足不了的情况下，用这个更方便。

```
# 需要导入类
from django.db import connection

## 不通过模型层来执行SQL查询
def my_custom_sql(self):
    with connection.cursor() as cursor:
        cursor.execute("select * from polls_choice")
        row = cursor.fetchone()
        print(row)
    return HttpResponse("不通过模型层执行SQL查询成功")
```

##### （3）导入Mysql的连接配置信息，执行SQL语句，不依赖Django。

定义公共类： conn_db.py

```
import pymysql

# 使用原生方式连接Mysql执行SQL语句
class Conn_Mysql():
    def __init__(self):
        self.db = pymysql.connect("zz.cn","knight","xxooxxooG_38","db_edu" )

    def exe_sql(self,sql):
        cursor = self.db.cursor()
        cursor.execute(sql)
        self.db.commit()
        data = cursor.fetchall()
        print(data)
```

视图函数如下：

```
# 通过导入公共类实现自定义SQL语句
def sql_org(request):
    sql = "INSERT into polls_book (title) VALUES ('yy')"
    # 实例化
    db = conn_db.Conn_Mysql()
    db.exe_sql(sql)
    return HttpResponse("使用pymysql执行SQL语句")
```

