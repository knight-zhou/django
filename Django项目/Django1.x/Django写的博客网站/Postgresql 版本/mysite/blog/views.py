#coding:utf-8
from django.shortcuts import render,HttpResponse
from blog.models import BlogsPost
from django.shortcuts import render_to_response

# Create your views here.

def index(request):
    blog_list = BlogsPost.objects.all()
    # return HttpResponse("this is index html")
    return render_to_response('index.html',{'posts':blog_list})

