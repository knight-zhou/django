# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response, get_object_or_404
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator


from asset.form import *
from asset.models import *
from asset.asset_info import *
from oms.mysql import db_operate
from oms import settings
from oms.models import *

# Create your views here.
############# 以下是host的相关信息###############################
def host_list(request):
    """
    List all Hosts
    """
    # return HttpResponse('xxx')

    user = request.user
    all_host = HostList.objects.all()
    paginator = Paginator(all_host, 10)

    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1

    try:
        all_host = paginator.page(page)
    except:
        all_host = paginator.page(paginator.num_pages)

    return render_to_response('host/host_list.html', {'all_host_list': all_host, 'page': page, 'paginator': paginator})


def host_list_manage(request,id=None):
    """
    Manage Host List
    """
    user = request.user
    if id:
        host_list = get_object_or_404(HostList, pk=id)
        action = 'edit'
        page_name = '编辑主机'
        db = db_operate()
        sql = 'select ip from oms_hostlist where id = %s' % (id)
        ret = db.mysql_command(settings.OMS_MYSQL,sql)
    else:
        host_list = HostList()
        action = 'add'
        page_name = '新增主机'

    # 删除主机
    if request.method == 'GET':
        delete = request.GET.get('delete')
        id = request.GET.get('id')
        # print ("#############: "+id)

        if delete:
            print "########"
            host_list = get_object_or_404(HostList, pk=id)
            host_list.delete()
            return HttpResponseRedirect(reverse("asset:host_list"))


    #编辑主机
    if request.method == 'POST':
        form = HostsListForm(request.POST,instance=host_list)
        operate = request.POST.get('operate')
        if form.is_valid():
            if action == 'add':
                form.save()
                return HttpResponseRedirect(reverse('asset:host_list'))
            if operate:
                if operate == 'update':
                    form.save()
                    return HttpResponseRedirect(reverse('asset:host_list'))
                else:
                    pass
    else:
        form = HostsListForm(instance=host_list)
    return render_to_response('host/host_manage.html',{"form": form,"page_name": page_name,"action": action,})

######################################### 以下是设备列表############################################
def network_device_list(request):
    """
    List all Network Device
    """

    user = request.user
    all_device = NetworkAsset.objects.all()
    paginator = Paginator(all_device, 10)

    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1

    try:
        all_device = paginator.page(page)
    except:
        all_device = paginator.page(paginator.num_pages)

    return render_to_response('device/device_list.html', {'all_device_list': all_device, 'page': page, 'paginator': paginator})


def network_device_discovery(request, id=None):
    """
    Manage Network Device
    """

    if id:
        device_list = get_object_or_404(NetworkAsset, pk=id)
        action = 'edit'
        page_name = '编辑设备'
    else:
        device_list = NetworkAsset()
        action = 'add'
        page_name = '新增设备'

    if request.method == 'POST':
        form = NetworkAssetForm(request.POST, instance=device_list)
        operate = request.POST.get('operate')
        if form.is_valid():
            if action == 'add':
                form.save()
                return HttpResponseRedirect(reverse('asset:network_device_list'))
            if operate:
                if operate == 'update':
                    form.save()
                    return HttpResponseRedirect(reverse('asset:network_device_list'))
                else:
                    pass
    else:
        form = NetworkAssetForm(instance=device_list)
    return render_to_response('device/device_manage.html',{"form": form,"page_name": page_name,"action": action,})

##########################################以下是idc信息列表 ##############################
def idc_asset_list(request):
    """
    List all IDC
    """

    user = request.user
    all_idc = IdcAsset.objects.all()
    paginator = Paginator(all_idc,10)

    try:
        page = int(request.GET.get('page','1'))
    except ValueError:
        page = 1

    try:
        all_idc = paginator.page(page)
    except :
        all_idc = paginator.page(paginator.num_pages)

    return render_to_response('idc/idc_list.html', {'all_idc_list': all_idc, 'page': page, 'paginator':paginator})

def idc_asset_manage(request,id=None):
    """
    Manage IDC
    """

    if id:
        idc_list = get_object_or_404(IdcAsset, pk=id)
        action = 'edit'
        page_name = '编辑IDC机房'
    else:
        idc_list = IdcAsset()
        action = 'add'
        page_name = '新增IDC机房'

    if request.method == 'POST':
        form = IdcAssetForm(request.POST,instance=idc_list)
        operate = request.POST.get('operate')
        if form.is_valid():
            if action == 'add':
                form.save()
                return HttpResponseRedirect(reverse('asset:idc_asset_list'))
            if operate:
                if operate == 'update':
                    form.save()
                    return HttpResponseRedirect(reverse('asset:idc_asset_list'))
                else:
                    pass
    else:
        form = IdcAssetForm(instance=idc_list)

    # return render_to_response('idc_manage.html',{"form": form,"page_name": page_name,"action": action,},context_instance=RequestContext(request))
    return render_to_response('idc/idc_manage.html',{"form": form,"page_name": page_name,"action": action,})