#coding:utf-8
from django.conf.urls import url
from .views import  *

urlpatterns = [
    url(r'^host_list/',host_list,name='host_list'),
    url(r'^add_host/',host_list_manage,name='add_host'),
    url(r'^host_delete/',host_list_manage,name='host_delete'),
    url(r'^host_manage/(?P<id>\d+)/$',host_list_manage,name='host_manage'),
    url(r'^device_list/',network_device_list,name='network_device_list'),
    url(r'^device_add/',network_device_discovery,name='add_device'),
    url(r'^idc_list/',idc_asset_list,name='idc_asset_list'),
    url(r'^add_idc/',idc_asset_manage,name='add_idc'),

]
