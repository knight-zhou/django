#coding:utf-8
from django.conf.urls import url,include
from django.contrib import admin
from . import  views

urlpatterns = [
    url(r'^oms/admin/', admin.site.urls),
    url(r'^$',views.index,name='index'),
    url(r'^deploy/',include('deploy.urls', namespace="deploy")),
    url(r'^asset/',include('asset.urls', namespace="asset")),
    url(r'^installed/',include('installed.urls', namespace="installed")),
    url(r'^monitor/',include('monitor.urls', namespace="monitor")),

]
