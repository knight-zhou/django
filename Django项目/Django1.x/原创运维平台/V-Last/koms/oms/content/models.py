# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Type(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, verbose_name=u"故障类型")

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = u'故障类型'



class Content(models.Model):
    fms_level = (
        (0, u"非常严重"),
        (1, u"严重"),
        (2, u"中等"),
        (3, u"一般"),
        (4, u"无影响"),
    )
    fms_status = (
        (0, u"处理中"),
        (1, u"已恢复"),
        (2, u"改进中"),
        (3, u"已完结"),
    )
    fms_improve = (
        (0, u"开发"),
        (1, u"运维"),
        (2, u"机房"),
        (3, u"网络运营商"),
        (4, u"第三方"),

    )
    fms_type = Type.objects.all().values_list('id', 'name')
    title = models.CharField(max_length=255, verbose_name=u'故障简述', unique=True)
    level = models.IntegerField(choices=fms_level, verbose_name=u'故障级别')
    type = models.ForeignKey(Type, related_name='fms_type', verbose_name=u'故障类型',null=True)        # 外键
    project = models.TextField(verbose_name=u'影响项目',null=True)
    effect = models.TextField(blank=True, verbose_name=u'故障影响')
    reasons = models.TextField(blank=True, verbose_name=u'故障原因',null=True)
    solution = models.TextField(blank=True, verbose_name=u'解决方案',null=True)
    status = models.IntegerField(choices=fms_status, verbose_name=u'故障状态')
    improve = models.IntegerField(choices=fms_improve, verbose_name=u'主导改进')
    content = models.TextField(blank=True, verbose_name=u'故障分析')
    start_time = models.DateTimeField(verbose_name=u'开始时间',default='2018-01-01 00:00:00')
    end_time = models.DateTimeField(verbose_name=u'结束时间',default='2018-01-01 00:00:00')
    ctime = models.DateTimeField(auto_now_add=True, verbose_name=u'创建时间')

    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = u'故障分析'
