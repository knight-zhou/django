#-*- coding: utf-8 -*-

from django import forms
from content.models import Content,Type
from django.contrib.auth.models import Group


class TypeForm(forms.ModelForm):
    class Meta:
        model = Type
        fields = '__all__'
        widgets = {
            'name' : forms.TextInput(attrs={'class':'form-control'}),
        }

class ContentForm(forms.ModelForm):

    class Meta:
        model = Content
        exclude = ('author',)
        description0 = '请按如下文本格式编写:\n1.整理知识，学习笔记\n2.发布日记，杂文，所见所想\n3.撰写发布技术文稿（代码支持)'
        description1 = '描叙故障发生的前前后后进行分析'
        description2 = '请按格式进行逐行进行描叙'
        description3 = '描述对哪些业务造成了影响'
        description4 = '2018-01-09 15:00:48'

        widgets = {
            'title' : forms.TextInput(attrs={'class':'form-control'}),
            'level' : forms.Select(attrs={'class':'form-control'}),
            'type' : forms.Select(attrs={'class':'form-control'}),
            'project' : forms.TextInput(attrs={'class':'form-control'}),
            'effect' : forms.Textarea(attrs={'class':'form-control','rows': '3','placeholder':description3}),
            'reasons' : forms.Textarea(attrs={'class':'form-control','rows': '5','placeholder': description1}),
            'solution' : forms.Textarea(attrs={'class':'form-control','rows': '7','placeholder': description0}),
            'improve' : forms.Select(attrs={'class':'form-control'}),
            'status' : forms.Select(attrs={'class':'form-control'}),
            'content' : forms.Textarea(),
        }


class GroupForm(forms.ModelForm):

    class Meta:
        model = Group
        fields = '__all__'
        widgets = {
            'name' : forms.TextInput(attrs={'class':'form-control'}),
        }



class ImagesUploadForm(forms.ModelForm):
    
    class  Meta:
        fields = '__all__'

