# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render_to_response
from django.http import  HttpResponse
from .models import InterfaceList
from monitorapi import Monitor
import json
import time

# Create your views here.
def zn_list(request):
    mapi=Monitor()
    all_inter = InterfaceList.objects.all()
    ret=""
    if request.method == 'POST':
        mk = request.POST.get('mk')
        xx = InterfaceList.objects.get(module=mk)
        url = xx.url
        h1 = xx.headers
        headers=eval(str(h1))
        data = xx.body
        ret =mapi.send_res(url,data,headers)
        code = json.loads(ret)
        errcode = code[u"errcode"]
        mtime = time.strftime("%Y-%m-%d %H:%M:%S")
        print mtime

        InterfaceList.objects.filter(module=mk).update(status=errcode,mod_time=mtime)   # update数据库


    return render_to_response('jk/zn_list.html',{'all_inter':all_inter,'ret':ret})


