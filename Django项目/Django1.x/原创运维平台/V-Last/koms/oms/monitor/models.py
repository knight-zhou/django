# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class InterfaceList(models.Model):
    module = models.CharField(max_length=20, verbose_name=u'模块')
    url= models.CharField(max_length=100, verbose_name=u'接口地址')
    headers = models.CharField(max_length=50,blank=True,default="{'content-type': 'application/json'}",verbose_name=u'头部')
    body = models.CharField(max_length=50, blank=True, verbose_name=u'请求参数')
    request_mode = models.CharField(max_length=10,default="http post",verbose_name=u'请求方式')
    update_interval = models.TextField(max_length=50, default="手工",verbose_name=u'请求频率')
    status = models.TextField(max_length=10,verbose_name=u'状态')
    response = models.TextField(max_length=200,verbose_name=u'返回结果')
    mod_time= models.DateTimeField('最后修改日期',auto_now=True)
    remark = models.TextField(max_length=50,verbose_name=u'备注')

    def __unicode__(self):
        return u'%s - %s - %s' % (self.status, self.response)

    class Meta:
        verbose_name = u'状态'
        verbose_name = u'返回结果'