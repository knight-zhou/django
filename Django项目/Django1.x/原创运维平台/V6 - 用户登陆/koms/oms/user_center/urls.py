#coding:utf-8
from django.conf.urls import url
from .views import  *

urlpatterns = [
    url(r'^$',login,name='login'),
    url(r'^sign_up/',sign_up,name='sign_up'),
    url(r'^not_found/',not_found,name='not_found'),

]
