# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.core.urlresolvers import reverse
from content.forms import ContentForm,TypeForm
from django.shortcuts import render_to_response, HttpResponseRedirect, HttpResponse, render
from django.db.models import Q
from .models import Content,Type
from django.db.models import Count


# Create your views here.



def fms_list(request):
    all_content = Content.objects.all()
    return render_to_response('fms/fms_list.html',{'all_content':all_content})



def fms_add(request):
    error = ""
    if request.method == "POST":
        title = Content.objects.filter(title=request.POST.get('title'))
        form_input = ContentForm(request.POST)
        data = form_input.data   # 表单传过来的数据
        if title:
            error = " 简述标题冲突!"
            print "##########"
        else:
            # print "写入数据库"
            Content.objects.create(
                title=data['title'],
                level=data['level'],
                project=data['project'],
                status=data['status'],
                start_time=data['start_time'],
                end_time=data['end_time'],
                improve=data['improve'],
                effect=data['effect'],
                reasons=data['reasons'],
                solution=data['solution'],
        )
            return HttpResponseRedirect(reverse("content:fms_list"))

    else:
        form_input = ContentForm()
    return render(request, 'fms/fms_add.html', {'request': request, 'form': form_input, 'error': error})


def fms_type(request):
    all_type = Type.objects.all()
    return render_to_response('fms/fms_type.html',{'all_type':all_type})



def fms_type_add(request):
    if request.method == "POST":
        form_input = TypeForm(request.POST)
        cc = Type.objects.all().count()
        data=form_input.data
        Type.objects.create(id=cc+1,name=data['name'])

        return HttpResponseRedirect(reverse("content:fms_type"))
    else:
        form_input=TypeForm()

    return render_to_response('fms/fms_type_add.html',{'form':form_input})