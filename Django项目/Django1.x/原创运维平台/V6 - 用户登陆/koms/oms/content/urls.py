#coding:utf-8
from django.conf.urls import url
from .views import  *

# 命名正则表达式组的语法是(?P<name>pattern)，其中name 是组的名称，pattern 是要匹配的模式
urlpatterns = [
    url(r'^add/',fms_add,name='fms_add'),
    url(r'^list/',fms_list,name='fms_list'),
    url(r'^type/',fms_type,name='fms_type'),
    url(r'^type_add',fms_type_add,name='fms_type_add'),
]
