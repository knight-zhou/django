#coding:utf-8
from __future__ import unicode_literals
import time
import os,shutil

from django.shortcuts import render,HttpResponse,render_to_response,get_object_or_404
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect,Http404
from django.urls import reverse
from django.template import RequestContext

from deploy.saltapi import SaltAPI
from oms import settings
from oms import  settings_local
from oms.models import *
from oms.mysql import db_operate
from asset.models import HostList
from deploy.code import Code_Work
from json_data import BuildJson
from deploy.form import ContactForm
from .login_ssh import *



# Create your views here.

#以下是部署管理操作

def module_deploy(request):
    """
    deploy (nginx/php/mysql..etc) module
    """
    ret = '编译请看编译日志，发布和最后编译返回结果请看这里'
    tgt='localhost'

    f = open('java_hard.txt', 'r')
    ver_all=[]
    module = set()
    for i in file.xreadlines(f):
        if not i.startswith('#'):               # 过滤'#' 开头的注释
            ii = i.strip().replace("\n", "")    # 去掉两边空格和换行符
            v = ii.split('|')[0]
            ver_all.append(ii)
            module.add(v)

    while '' in ver_all:       # 列表去除空元素
        ver_all.remove('')

    module.remove("")      # 集合去除空元素
    f.close()

    if request.method == 'POST':
        if request.POST.get('mod') == 'bianyi':
            print "进行编译..."
        elif request.POST.get('mod') == 'fabu':
            print "执行发布"
        else:
            pass
        # bianyi= request.POST.get('mod')
        # print bianyi


    return render_to_response('salt/salt_module_deploy.html',{'ret':ret,'tgt':tgt,'ver_all':ver_all,'module':module})


def change_ver(request):
# 模块值传到前台界面
    module = set()
    with open('java_hard.txt', 'r') as f:
        for line in f.xreadlines():
            if not line.startswith('#'):
                ii = line.strip().replace("\n", "")
                v = ii.split('|')[0]
                module.add(v)

    module.remove("")

    if request.method == 'POST':
        module=request.POST.get('module')
        ver=request.POST.get('ver')
        env=request.POST.get('env')   # 都能正常获取到值
        new_l = module + '|' + ver + '|' + env

        form = ContactForm(request.POST)                 # 验证表单必须要同时有值
        if form.is_valid():
            # print module
            # print ver
############修改文件#############
            # 删除行
            with open('java_hard.txt', 'r') as f:
                lines = f.readlines()
                # print (lines)

            with open('java_hard.txt', 'w') as f_w:
                for line in lines:
                    if module in line and env in line:
                        continue
                    f_w.write(line)

            # 追加行
            with open('java_hard.txt', 'a') as f_a:
                f_a.write('\n')
                f_a.write(new_l)
##################################################
            return HttpResponseRedirect(reverse('deploy:module_deploy'))
        else:
            return render(request, "salt/change_ver.html", {"error": form.errors,"form":form})  # 表单不填写就提示报错信息

    return render_to_response('salt/change_ver.html',{'module':module})


def remote_execution(request):
    """
    remote command execution
    """
    ret = "禁止使用  shutdown  init  reboot  等危险指令...."
    danger = ('rm', 'reboot', 'init ', 'shutdown','passwd')
    if request.method=='POST':
        tgt = request.POST.get('tgt')
        arg = request.POST.get('arg')
        argcheck = arg not in danger
        if argcheck:
            sapi = SaltAPI(url=settings.SALT_API['url'], username=settings.SALT_API['user'],password=settings.SALT_API['password'])
            ret = sapi.remote_execution(tgt, 'cmd.run', arg)
        else:
            ret='亲，命令很危险, 你这样子管理员可不开森了.....'

    return render_to_response('salt/salt_remote_execution.html', {'ret': ret})


########################################### 代码发布功能################################
def code_deploy(request):
    """
    Pull code for building, pushed to the server
    """

    ret = '此功能尚不对外开放......'
    host = {'ga': 'test-01', 'beta': 'localhost.localdomain'}
    user = request.user
    if request.method == 'POST':
        action = request.get_full_path().split('=')[1]
        if action == 'push':
            pro = request.POST.get('project')
            url = request.POST.get('url')
            ver = request.POST.get('version')
            env = request.POST.get('env')
            capi = Code_Work(pro=pro, url=url, ver=ver)
            data = {pro: {'ver': ver}}
            obj = capi.work()                               # 构建rpm包
            if obj['comment'][0]['result'] and obj['comment'][1]['result'] and obj['comment'][2]['result']:
                json_api = BuildJson()
                json_api.build_data(host[env], data)        # 刷新pillar数据，通过deploy下发SLS执行代码发布
                sapi = SaltAPI(url=settings.SALT_API['url'], username=settings.SALT_API['user'],
                               password=settings.SALT_API['password'])
                if env == 'beta':
                    jid = sapi.target_deploy('beta', 'deploy.' + pro)
                elif env == 'ga':
                    jid = sapi.target_deploy('tg', 'deploy.' + pro)
                else:
                    jid = sapi.target_deploy('beta', 'deploy.' + pro)
                time.sleep(8)
                db = db_operate()
                sql = 'select returns from salt_returns where jid=%s'
                ret = db.select_table(settings.RETURNS_MYSQL, sql, str(jid))  # 通过jid获取执行结果
    return render_to_response('salt/code_deploy.html',{'ret': ret})