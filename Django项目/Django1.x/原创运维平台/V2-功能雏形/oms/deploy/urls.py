#coding:utf-8
from django.conf.urls import url
from .views import  *
from .views_key import *

urlpatterns = [
    url(r'^key_list/',salt_key_list,name='key_list'),
    url(r'^key_delete/',salt_delete_key,name='key_delete'),
    url(r'^key_accept/',salt_accept_key,name='accept_key'),
    url(r'^module_deploy/',module_deploy,name='module_deploy'),
    url(r'^remote_execution/',remote_execution,name='remote_execution'),
    url(r'^code_deploy/',code_deploy,name='code_deploy'),
    url(r'^change_ver/',change_ver,name='change_ver'),

]
