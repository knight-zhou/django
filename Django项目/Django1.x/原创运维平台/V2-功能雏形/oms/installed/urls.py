# coding:utf-8
from django.conf.urls import url,include
from . import  views

urlpatterns = [
    url(r'^system_install_list', views.system_install_list, name='system_install_list'),
    url(r'^system_install_record', views.system_install_record, name='system_install_record'),

]
