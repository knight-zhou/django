#coding:utf-8
from django.conf.urls import url
from django.contrib import admin
from . import  views

urlpatterns = [
    url(r'^oms/admin/', admin.site.urls),
    url(r'^$',views.index,name='index'),
]
