## django  url reverse(反解析)
1.8  还是1.10 强制要求加app_name，准确形式如下:  app_name:url_name *参数
```
<a href="{% url 'deploy:key_list' %}">Salt认证</a>
```

## 模型应用到数据库
```
python manage.py makemigrations  		#记录models.py的改动
python manage.py migrate   				#将改动作用到数据库文件
```

## 404 异常页面
from django.http import Http404
```
## 测试404 页面
def test(request):
    # return HttpResponse('xx')
    raise Http404("找不到页面....")
```

## Django中的Form类使用时一般有两种功能：
* 生成html标签
* 验证输入内容

## Django的ModelForm简单用法
```
Django中内置了Form和Model两个类，有时候页面的表单form类与Model类是一一对应，因此分别定义Form类和Model类会比较麻烦，最简单的方式就是通过Model来生成一个Form类，Django内置的ModelForm就是为此而生的
```