SALT_API = {"url": "https://192.168.30.42:8888",
            "user": "saltapi",
            "password": "password"
            }

Cobbler_API = {"url": "",
            "user": "",
            "password": ""
            }

# salt result
RETURNS_MYSQL = {"host": "localhost",
               "port": 3306,
               "database": "salt",
               "user": "knight",
               "password": "knight"
                }

SERVICE = {"nginx": "nginx",
           "php": "php",
           "mysql": "mysql",
           "sysinit": "sysinit",
           "logstash": "logstash",
           "zabbix": "zabbix",
           "redis": "redis",
           "memcached": "memcached"
          }

OMS_MYSQL = {"host": "localhost",
               "port": 3306,
               "database": "oms",
               "user": "knight",
               "password": "knight"
                }
